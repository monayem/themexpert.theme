$(document).ready(function() {


    //Hero Section 
    var heroSec = $('.hero-section') ;
    var hArea   = $('.highlight-area')
    var headerH = $('header').height() ;
    heroSec.css({'top':-headerH-20}) ;
    hArea.css({'margin-top':-headerH-20}) ;

    // Client Slider
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 3,
                nav: false
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 6,
                nav: false,
                loop: false
            }
        }
    })

    //FAQ BOX

    var faqBoxItem = $('.faq-box-item');
    var faqBoxTitle = $('.faq-box-title');
    var faqBoxDetails = $('.faq-box-details');
    faqBoxTitle.click(function() {

        $(this).parent().find(faqBoxDetails).slideToggle(500);

        if (!$(this).hasClass('faq-active')) {
            $(this).addClass('faq-active');
        } else {
            $(this).removeClass('faq-active');
        }

    })


    // LOAD EFFECT

    $(window).on("scroll", function() {

        var winScr      = $(window).scrollTop();
        var winW        = $(window).width();
        var header      = $("header");
        var featureSec  = $(".feature-section");
        var effSec      = $(".efficiency-section");
        var insSec      = $(".inspirational-quotes");
        var hSec        = $(".hero-section");
        var footer      = $("footer");

        loadAnimation(header);
        loadAnimation(featureSec);
        loadAnimation(effSec);
        loadAnimation(insSec);
        loadAnimation(hSec);
        loadAnimation(footer);

        function loadAnimation(selector) {

            var WindowH = $(window).height();
            var wTP = $(window).scrollTop();
            var wBP = WindowH + wTP;
            var pbH = selector.height();
            var pbTP = selector.offset().top;
            var pbBP = pbH + pbTP;
            if (wBP >= pbTP && pbBP >= wTP) {
                selector.stop().animate({ 'opacity': '1' }, 700);
            } else {
                selector.stop().animate({ 'opacity': '0' }, 700);
            }
        }


    })




})